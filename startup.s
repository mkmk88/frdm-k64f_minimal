.cpu cortex-m4
.thumb

.section .vectors, "a"
    .word _stack_top
    .word _reset

.section .flash_config, "a"
    .long 0xFFFFFFFF
    .long 0xFFFFFFFF
    .long 0xFFFFFFFF
    .long 0xFFFFFFFE

.section .text
.thumb_func
.global _reset
_reset:
    bl init
    bl main
