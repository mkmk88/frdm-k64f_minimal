# README #

This is a basic GCC setup for Freescale Freedom K64F board (http://www.freescale.com/products/arm-processors/kinetis-cortex-m/k-series/k6x-ethernet-mcus/freescale-freedom-development-platform-for-kinetis-k64-k63-and-k24-mcus:FRDM-K64F). 

There are 3 branches:

* master: minimal needed SW to light LED and write simple native programs.
* stdlib: it's the same as master but you can use stdlib (tested with newlib). The startup code will use hooks run by crt0.s to initialize watchdog and .data section. The .bss section will be zeroed automatically by crt0.s.
* stdlib_no_crt0: it's the same as stdlib but instead of using crt0.s from newlib the simple startup code is provided. Additionally, malloc() support is added.

### How do I get set up? ###

* Clone this repository.
* Get ARM GCC toolchain https://launchpad.net/gcc-arm-embedded.
* Add ARM GCC toolchain to path.
* Go to repo's root directory and run "make". Two files should be generated: simple.elf and simple.bin.
* Copy simple.bin to the board. Blue LED should light constantly.

### Who do I talk to? ###

* Michal Kowalczyk kowalczykmichal88+bb@gmail.com
* http://fixbugfix.blogspot.com/