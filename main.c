#define SIM_SCGC5 (*(volatile int *)0x40048038)
#define SIM_SCGC5_PORTB 10

#define PORTB_PCR21 (*(volatile int *)0x4004A054)
#define PORTB_PCR21_MUX 8

#define GPIOB_PDDR (*(volatile int *)0x400FF054)
#define PIN_N 21

int main()
{
    /* Enable clocks. */
    SIM_SCGC5 |= 1 << SIM_SCGC5_PORTB;
    /* Configure pin 21 as GPIO. */
    PORTB_PCR21 |= 1 << PORTB_PCR21_MUX;
    /* Configure GPIO pin 21 as output.
     * It will have a default output value set
     * to 0, so LED will light (negative logic).
     */
    GPIOB_PDDR |= 1 << PIN_N;

    while(1);

    return 0;
}
