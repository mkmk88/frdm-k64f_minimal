CC=arm-none-eabi-gcc
OBJCPY=arm-none-eabi-objcopy
CFLAGS=-Wall -Wextra -mthumb -mcpu=cortex-m4 -nostdlib -g

all:
	$(CC) startup.s startup.c main.c $(CFLAGS) -T k64f.ld -o simple.elf
	$(OBJCPY) simple.elf simple.bin -O binary

clean:
	rm simple.*
